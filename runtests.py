# Runs the unit tests for dlmontepython

import sys 
import unittest
import os
  
loader = unittest.TestLoader()
tests = loader.discover(os.getcwd())
testRunner = unittest.runner.TextTestRunner()
testRunner.run(tests)


