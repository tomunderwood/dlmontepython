"""Test cases for fep module"""

import unittest

import dlmontepython.fep.fep as fep
import numpy as np



class TestFepMethane(unittest.TestCase):

    """Tests for fep module based on a previous set of calculations for methane"""

    def test_methane(self):

        # Chemical potential the simulation was performed at, and range to consider in
        # search for coexistence
        mu=-38.046
        mulbound= mu - abs(mu)*0.001
        muubound= mu + abs(mu)*0.001
        # kT (kJ/mol)
        kt=0.0083144621*300
        # Cross-sectional area of simulation box (angstrom^2)
        area=20**2
        # Volume of simulation box (angstrom^3)
        volume=20**3
        # Number of particles in system delineating the gas and liquid phases
        nthresh=150
        # Free energy profile file name
        filename="FEDDAT_ch4"

        # The test in earnest...

        prefactor=29915.0757/volume

        # Load free energy profile
        op, fe = fep.from_file(filename)
        
        # Test expected_op

        op1, op2 = fep.expected_op(op, fe, op_thresh=nthresh)
        np.testing.assert_almost_equal( op1, 0.0019812128546976213, decimal=10 )
        np.testing.assert_almost_equal( op2, 265.0190784012044, decimal=10 )
        
        # Test reweight_to_coexistence

        # Reweight to coexistence automatically - no tail corrections
        mubeta_opt, p_opt, fe_opt = fep.reweight_to_coexistence(op, fe, mu/kt, mulbound/kt, muubound/kt, op_thresh=nthresh)
        np.testing.assert_almost_equal(mubeta_opt*kt, -38.078381351909904, decimal=10)
        np.testing.assert_almost_equal(p_opt, 0.500003030195233, decimal=10)
        op1, op2 = fep.expected_op(op, fe_opt, op_thresh=nthresh)
        np.testing.assert_almost_equal( op1, 0.0019554772337082257, decimal=10 )
        np.testing.assert_almost_equal( op2, 264.7904020944669, decimal=10 )
        
        # Test vapour_pressure and surface_tension

        n1 = op1
        n2 = op2

        # The below pressure is in (kJ/mol)/A^3        
        pressure = fep.vapour_pressure(op, fe_opt, nthresh, kt, volume)
        # Convert pressure to bar
        pressure = pressure * 100000 / 6.02214076
        np.testing.assert_almost_equal(pressure, 0.010089720634962785, decimal=10)

        # The below tension is in (kJ/mol)/A^2
        tension = fep.surface_tension(op, fe_opt, nthresh, kt, area)
        # Convert tension to Nm^-1
        tension = tension / 6.02214076
        np.testing.assert_almost_equal(tension, 0.058858866059614164)




if __name__ == '__main__':

    unittest.main()
